<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="jauery.js"></script>
</head>
<body>
    <textarea id="txt"></textarea>
    <div id="count">0</div>
    <script>
        $(function(){
            $('#txt').keyup(function(){
                let a = $(this).val().length;
                $('#count').html(a);
            })
        })
    </script>
</body>
</html>