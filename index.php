<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AJAX</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>Add Recorde</h1>
                <label>Username</label>
                <input type="text" id="username" class="form-control" />
                <label>Password</label>
                <input type="password" id="password" class="form-control" />
                <label>Full Name</label>
                <input type="text" id="fullname" class="form-control" />
                <input type="button" class="btn btn-success" value="Submit" onclick="AddUser()" />
            </div>
            <div class="col-md-6">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Full Name</th>
                            <th>Username</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="mydata">
                    
                    </tbody>
                </table>
            </div>
            <div class="modal" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Change Password</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <label>New Password</label>
                        <input type='text' class="form-control" id='newpass' />
                        <input type="hidden" id="userid" />
                        <input type='button' value='Change' class="btn btn-success mt-3" onclick="changepassmain()" />
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                    </div>
                </div>
                </div>
        </div>
    </div>
    <script>
        function AddUser(){
            let username = $('#username').val();
            let password = $('#password').val();
            let fullname = $('#fullname').val();
            $.ajax({
                url:'ajax.php',
                type:'post',
                data:{user:username,pass:password,fullname:fullname},
                success:function(data){
                    alert(data);
                    getUsers();
                }
            })
        }
        function getUsers(){
            $.ajax({
                url:'ajax.php',
                type:'post',
                data:{getall:''},
                success:function(data){
                    $('#mydata').html(data);
                }
            });
        }
        getUsers();
        function deleterecord(id){
            $.ajax({
                url:'ajax.php',
                type:'post',
                data:{deleteid:id},
                success:function(data){
                    alert(data);
                    getUsers();
                }
            })
        }
        function editrecord(id, sr){
            $.ajax({
                url:'ajax.php',
                type:'post',
                data:{editdata:id},
                success:function(data){
                    let row = '<td>'+sr+'</td>';
                    row += "<td><input type='text' id='fname"+id+"' value='"+data.fullname+"' /></td>";
                    row += "<td><input type='text' id='uname"+id+"' value='"+data.username+"' /></td>";
                    row += "<td><button class='btn btn-success' onclick='saveedit("+id+")'>Save</button></td>";
                    $('#edit'+id).html(row);
                }
            })
        }
        function saveedit(id){
            let name = $('#fname'+id).val();
            let user = $('#uname'+id).val();
            $.ajax({
                url:'ajax.php',
                type:'post',
                data:{editname:name,editusername:user,editid:id},
                success: function(data){
                    getUsers();
                }
            })
        }
        function changepass(id){
            $('#userid').val(id);
        }
        function changepassmain(){
            let userid = $('#userid').val();
            let newpass = $('#newpass').val();
            $.ajax({
                url:'ajax.php',
                type:'post',
                data:{changeid:userid, newpass:newpass},
                success: function(data){
                    alert(data);
                }
            });
        }
    </script>
</body>
</html>